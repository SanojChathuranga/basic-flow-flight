package system.pages;

public class BookingEnginePage {

	boolean countryResidenceLabel			= false;
	String	countryResidenceLabelText		= "";
	boolean isFlightDefaultSelected			= false;
	boolean tripTypeMainLabel				= false;
	String	tripTypeMainLabelTxt			= "";
	boolean tripTypeRoundTripLabel			= false;
	String	tripTypeRoundTripLabelTxt		= "";
	boolean tripTypeRoundTripRadButton		= false;
	boolean tripTypeOneWayLabel				= false;
	boolean tripTypeOneWayRadButton			= false;
	String	tripTypeOneWayLabelTxt			= "";
	boolean tripTypeMultiLabel				= false;
	boolean tripTypeMultiRadButton			= false;
	String	tripTypeMultiLabelTxt			= "";
	boolean isSelectedRoundTripDefault		= false;
	boolean whereGoingLabel					= false;
	String	whereGoingLabelTxt				= "";
	boolean fromLabel						= false;
	String	fromLabelTxt					= "";
	boolean fromTxtBox						= false;
	boolean toLabel							= false;
	String	toLabelTxt						= "";
	boolean toTxtBox						= false;
	boolean departureDateLabel				= false;
	String	departureDateLabelTxt			= "";
	boolean departureDatePicker				= false;
	boolean departureTimeLabel				= false;
	String	departureTimeLabelTxt			= "";
	boolean departureTimeDrpDwn				= false;
	int		departureTimeDrpDwnSize			= 0;
	boolean returnDateLabel					= false;
	String	returnDateLabelTxt				= "";
	boolean returnDatePicker				= false;
	boolean returnTimeLabel					= false;
	String	returnTimeLabelTxt				= "";
	boolean returnTimeDrpDwn				= false;
	int		returnTimeDrpDwnSize			= 0;
	boolean howManyPeopleLabel				= false;
	String	howManyPeopleLabelTxt			= "";
	boolean adultsLabel						= false;
	String	adultsLabelTxt					= "";
	boolean childrenLabel					= false;
	String	childrenLabelTxt				= "";
	boolean infantLabel						= false;
	String	infantLabelTxt					= "";
	int		adultDropdownSize				= 0;
	int		childrenDropdownSize			= 0;
	int		infantDropdownSize				= 0;
	int		childAgeDrodownCount			= 0;
	int		childAgeDropdowns				= 0;
	boolean prefferedAirLineLabel			= false;
	String	prefferedAirLineLabelTxt		= "";
	boolean prefferedAirLineTxtBox			= false;
	boolean classLabel						= false;
	String	classLabelTxt					= "";
	boolean classDrpDwn						= false;
	int		classDrpDwnSize					= 0;
	boolean preferredCurrencyLabel			= false;
	String	preferredCurrencyLabelTxt		= "";
	boolean preferredCurrencyDrpDwn			= false;
	boolean promotionCodeLabel				= false;
	String	promotionCodeLabelTxt			= "";
	boolean promotionCodeTxtBox				= false;
	boolean preferNonStopFlightLabel		= false;
	String	preferNonStopFlightLabelTxt		= "";
	boolean preferNonStopFlightChkBox		= false;
	boolean additionalOptionTxt				= false;
	String	additionalOptionTxtBeforeClick	= "";
	String	additionalOptionTxtAfterClick	= "";
	boolean searchButton					= false;
	
	
	
	public boolean isClassDrpDwn() {
		return classDrpDwn;
	}
	public void setClassDrpDwn(boolean classDrpDwn) {
		this.classDrpDwn = classDrpDwn;
	}
	public int getClassDrpDwnSize() {
		return classDrpDwnSize;
	}
	public void setClassDrpDwnSize(int classDrpDwnSize) {
		this.classDrpDwnSize = classDrpDwnSize;
	}
	public boolean isCountryResidenceLabel() {
		return countryResidenceLabel;
	}
	public void setCountryResidenceLabel(boolean countryResidenceLabel) {
		this.countryResidenceLabel = countryResidenceLabel;
	}
	public String getCountryResidenceLabelText() {
		return countryResidenceLabelText;
	}
	public void setCountryResidenceLabelText(String countryResidenceLabelText) {
		this.countryResidenceLabelText = countryResidenceLabelText;
	}
	public int getReturnTimeDrpDwnSize() {
		return returnTimeDrpDwnSize;
	}
	public String getTripTypeMainLabelTxt() {
		return tripTypeMainLabelTxt;
	}
	public void setTripTypeMainLabelTxt(String tripTypeMainLabelTxt) {
		this.tripTypeMainLabelTxt = tripTypeMainLabelTxt;
	}
	public String getTripTypeRoundTripLabelTxt() {
		return tripTypeRoundTripLabelTxt;
	}
	public void setTripTypeRoundTripLabelTxt(String tripTypeRoundTripLabelTxt) {
		this.tripTypeRoundTripLabelTxt = tripTypeRoundTripLabelTxt;
	}
	public String getTripTypeOneWayLabelTxt() {
		return tripTypeOneWayLabelTxt;
	}
	public void setTripTypeOneWayLabelTxt(String tripTypeOneWayLabelTxt) {
		this.tripTypeOneWayLabelTxt = tripTypeOneWayLabelTxt;
	}
	public String getTripTypeMultiLabelTxt() {
		return tripTypeMultiLabelTxt;
	}
	public void setTripTypeMultiLabelTxt(String tripTypeMultiLabelTxt) {
		this.tripTypeMultiLabelTxt = tripTypeMultiLabelTxt;
	}
	public String getWhereGoingLabelTxt() {
		return whereGoingLabelTxt;
	}
	public void setWhereGoingLabelTxt(String whereGoingLabelTxt) {
		this.whereGoingLabelTxt = whereGoingLabelTxt;
	}
	public String getFromLabelTxt() {
		return fromLabelTxt;
	}
	public void setFromLabelTxt(String fromLabelTxt) {
		this.fromLabelTxt = fromLabelTxt;
	}
	public String getToLabelTxt() {
		return toLabelTxt;
	}
	public void setToLabelTxt(String toLabelTxt) {
		this.toLabelTxt = toLabelTxt;
	}
	public String getDepartureDateLabelTxt() {
		return departureDateLabelTxt;
	}
	public void setDepartureDateLabelTxt(String departureDateLabelTxt) {
		this.departureDateLabelTxt = departureDateLabelTxt;
	}
	public String getDepartureTimeLabelTxt() {
		return departureTimeLabelTxt;
	}
	public void setDepartureTimeLabelTxt(String departureTimeLabelTxt) {
		this.departureTimeLabelTxt = departureTimeLabelTxt;
	}
	public String getReturnDateLabelTxt() {
		return returnDateLabelTxt;
	}
	public void setReturnDateLabelTxt(String returnDateLabelTxt) {
		this.returnDateLabelTxt = returnDateLabelTxt;
	}
	public String getReturnTimeLabelTxt() {
		return returnTimeLabelTxt;
	}
	public void setReturnTimeLabelTxt(String returnTimeLabelTxt) {
		this.returnTimeLabelTxt = returnTimeLabelTxt;
	}
	public String getHowManyPeopleLabelTxt() {
		return howManyPeopleLabelTxt;
	}
	public void setHowManyPeopleLabelTxt(String howManyPeopleLabelTxt) {
		this.howManyPeopleLabelTxt = howManyPeopleLabelTxt;
	}
	public String getAdultsLabelTxt() {
		return adultsLabelTxt;
	}
	public void setAdultsLabelTxt(String adultsLabelTxt) {
		this.adultsLabelTxt = adultsLabelTxt;
	}
	public String getChildrenLabelTxt() {
		return childrenLabelTxt;
	}
	public void setChildrenLabelTxt(String childrenLabelTxt) {
		this.childrenLabelTxt = childrenLabelTxt;
	}
	public String getInfantLabelTxt() {
		return infantLabelTxt;
	}
	public void setInfantLabelTxt(String infantLabelTxt) {
		this.infantLabelTxt = infantLabelTxt;
	}
	public String getPrefferedAirLineLabelTxt() {
		return prefferedAirLineLabelTxt;
	}
	public void setPrefferedAirLineLabelTxt(String prefferedAirLineLabelTxt) {
		this.prefferedAirLineLabelTxt = prefferedAirLineLabelTxt;
	}
	public String getClassLabelTxt() {
		return classLabelTxt;
	}
	public void setClassLabelTxt(String classLabelTxt) {
		this.classLabelTxt = classLabelTxt;
	}
	public String getPreferredCurrencyLabelTxt() {
		return preferredCurrencyLabelTxt;
	}
	public void setPreferredCurrencyLabelTxt(String preferredCurrencyLabelTxt) {
		this.preferredCurrencyLabelTxt = preferredCurrencyLabelTxt;
	}
	public String getPromotionCodeLabelTxt() {
		return promotionCodeLabelTxt;
	}
	public void setPromotionCodeLabelTxt(String promotionCodeLabelTxt) {
		this.promotionCodeLabelTxt = promotionCodeLabelTxt;
	}
	public String getPreferNonStopFlightLabelTxt() {
		return preferNonStopFlightLabelTxt;
	}
	public void setPreferNonStopFlightLabelTxt(String preferNonStopFlightLabelTxt) {
		this.preferNonStopFlightLabelTxt = preferNonStopFlightLabelTxt;
	}
	public boolean isPreferNonStopFlightChkBox() {
		return preferNonStopFlightChkBox;
	}
	public void setPreferNonStopFlightChkBox(boolean preferNonStopFlightChkBox) {
		this.preferNonStopFlightChkBox = preferNonStopFlightChkBox;
	}
	public boolean isFlightDefaultSelected() {
		return isFlightDefaultSelected;
	}
	public void setFlightDefaultSelected(boolean isFlightDefaultSelected) {
		this.isFlightDefaultSelected = isFlightDefaultSelected;
	}
	public boolean isTripTypeMainLabel() {
		return tripTypeMainLabel;
	}
	public void setTripTypeMainLabel(boolean tripTypeMainLabel) {
		this.tripTypeMainLabel = tripTypeMainLabel;
	}
	public boolean isTripTypeRoundTripLabel() {
		return tripTypeRoundTripLabel;
	}
	public void setTripTypeRoundTripLabel(boolean tripTypeRoundTripLabel) {
		this.tripTypeRoundTripLabel = tripTypeRoundTripLabel;
	}
	public boolean isTripTypeRoundTripRadButton() {
		return tripTypeRoundTripRadButton;
	}
	public void setTripTypeRoundTripRadButton(boolean tripTypeRoundTripRadButton) {
		this.tripTypeRoundTripRadButton = tripTypeRoundTripRadButton;
	}
	public boolean isTripTypeOneWayLabel() {
		return tripTypeOneWayLabel;
	}
	public void setTripTypeOneWayLabel(boolean tripTypeOneWayLabel) {
		this.tripTypeOneWayLabel = tripTypeOneWayLabel;
	}
	public boolean isTripTypeOneWayRadButton() {
		return tripTypeOneWayRadButton;
	}
	public void setTripTypeOneWayRadButton(boolean tripTypeOneWayRadButton) {
		this.tripTypeOneWayRadButton = tripTypeOneWayRadButton;
	}
	public boolean isTripTypeMultiLabel() {
		return tripTypeMultiLabel;
	}
	public void setTripTypeMultiLabel(boolean tripTypeMultiLabel) {
		this.tripTypeMultiLabel = tripTypeMultiLabel;
	}
	public boolean isTripTypeMultiRadButton() {
		return tripTypeMultiRadButton;
	}
	public void setTripTypeMultiRadButton(boolean tripTypeMultiRadButton) {
		this.tripTypeMultiRadButton = tripTypeMultiRadButton;
	}
	public boolean isSelectedRoundTripDefault() {
		return isSelectedRoundTripDefault;
	}
	public void setSelectedRoundTripDefault(boolean isSelectedRoundTripDefault) {
		this.isSelectedRoundTripDefault = isSelectedRoundTripDefault;
	}
	public boolean isWhereGoingLabel() {
		return whereGoingLabel;
	}
	public void setWhereGoingLabel(boolean whereGoingLabel) {
		this.whereGoingLabel = whereGoingLabel;
	}
	public boolean isFromLabel() {
		return fromLabel;
	}
	public void setFromLabel(boolean fromLabel) {
		this.fromLabel = fromLabel;
	}
	public boolean isFromTxtBox() {
		return fromTxtBox;
	}
	public void setFromTxtBox(boolean fromTxtBox) {
		this.fromTxtBox = fromTxtBox;
	}
	public boolean isToLabel() {
		return toLabel;
	}
	public void setToLabel(boolean toLabel) {
		this.toLabel = toLabel;
	}
	public boolean isToTxtBox() {
		return toTxtBox;
	}
	public void setToTxtBox(boolean toTxtBox) {
		this.toTxtBox = toTxtBox;
	}
	public boolean isDepartureDateLabel() {
		return departureDateLabel;
	}
	public void setDepartureDateLabel(boolean departureDateLabel) {
		this.departureDateLabel = departureDateLabel;
	}
	public boolean isDepartureDatePicker() {
		return departureDatePicker;
	}
	public void setDepartureDatePicker(boolean departureDatePicker) {
		this.departureDatePicker = departureDatePicker;
	}
	public boolean isDepartureTimeLabel() {
		return departureTimeLabel;
	}
	public void setDepartureTimeLabel(boolean departureTimeLabel) {
		this.departureTimeLabel = departureTimeLabel;
	}
	public boolean isDepartureTimeDrpDwn() {
		return departureTimeDrpDwn;
	}
	public void setDepartureTimeDrpDwn(boolean departureTimeDrpDwn) {
		this.departureTimeDrpDwn = departureTimeDrpDwn;
	}
	public int getDepartureTimeDrpDwnSize() {
		return departureTimeDrpDwnSize;
	}
	public void setDepartureTimeDrpDwnSize(int departureTimeDrpDwnSize) {
		this.departureTimeDrpDwnSize = departureTimeDrpDwnSize;
	}
	public boolean isReturnDateLabel() {
		return returnDateLabel;
	}
	public void setReturnDateLabel(boolean returnDateLabel) {
		this.returnDateLabel = returnDateLabel;
	}
	public boolean isReturnDatePicker() {
		return returnDatePicker;
	}
	public void setReturnDatePicker(boolean returnDatePicker) {
		this.returnDatePicker = returnDatePicker;
	}
	public boolean isReturnTimeLabel() {
		return returnTimeLabel;
	}
	public void setReturnTimeLabel(boolean returnTimeLabel) {
		this.returnTimeLabel = returnTimeLabel;
	}
	public boolean isReturnTimeDrpDwn() {
		return returnTimeDrpDwn;
	}
	public void setReturnTimeDrpDwn(boolean returnTimeDrpDwn) {
		this.returnTimeDrpDwn = returnTimeDrpDwn;
	}
	public int isReturnTimeDrpDwnSize() {
		return returnTimeDrpDwnSize;
	}
	public void setReturnTimeDrpDwnSize(int returnTimeDrpDwnSize) {
		this.returnTimeDrpDwnSize = returnTimeDrpDwnSize;
	}
	public boolean isHowManyPeopleLabel() {
		return howManyPeopleLabel;
	}
	public void setHowManyPeopleLabel(boolean howManyPeopleLabel) {
		this.howManyPeopleLabel = howManyPeopleLabel;
	}
	public boolean isAdultsLabel() {
		return adultsLabel;
	}
	public void setAdultsLabel(boolean adultsLabel) {
		this.adultsLabel = adultsLabel;
	}
	public boolean isChildrenLabel() {
		return childrenLabel;
	}
	public void setChildrenLabel(boolean childrenLabel) {
		this.childrenLabel = childrenLabel;
	}
	public boolean isInfantLabel() {
		return infantLabel;
	}
	public void setInfantLabel(boolean infantLabel) {
		this.infantLabel = infantLabel;
	}
	public int getAdultDropdownSize() {
		return adultDropdownSize;
	}
	public void setAdultDropdownSize(int adultDropdownSize) {
		this.adultDropdownSize = adultDropdownSize;
	}
	public int getChildrenDropdownSize() {
		return childrenDropdownSize;
	}
	public void setChildrenDropdownSize(int childrenDropdownSize) {
		this.childrenDropdownSize = childrenDropdownSize;
	}
	public int getInfantDropdownSize() {
		return infantDropdownSize;
	}
	public void setInfantDropdownSize(int infantDropdownSize) {
		this.infantDropdownSize = infantDropdownSize;
	}
	public int getChildAgeDrodownCount() {
		return childAgeDrodownCount;
	}
	public void setChildAgeDrodownCount(int childAgeDrodownCount) {
		this.childAgeDrodownCount = childAgeDrodownCount;
	}
	public int getChildAgeDropdowns() {
		return childAgeDropdowns;
	}
	public void setChildAgeDropdowns(int childAgeDropdowns) {
		this.childAgeDropdowns = childAgeDropdowns;
	}
	public boolean isPrefferedAirLineLabel() {
		return prefferedAirLineLabel;
	}
	public void setPrefferedAirLineLabel(boolean prefferedAirLineLabel) {
		this.prefferedAirLineLabel = prefferedAirLineLabel;
	}
	public boolean isPrefferedAirLineTxtBox() {
		return prefferedAirLineTxtBox;
	}
	public void setPrefferedAirLineTxtBox(boolean prefferedAirLineTxtBox) {
		this.prefferedAirLineTxtBox = prefferedAirLineTxtBox;
	}
	public boolean isClassLabel() {
		return classLabel;
	}
	public void setClassLabel(boolean classLabel) {
		this.classLabel = classLabel;
	}
	public boolean isClassDrpDwnSize() {
		return classDrpDwn;
	}
	public void setClassDrpDwnSize(boolean classDrpDwnSize) {
		this.classDrpDwn = classDrpDwnSize;
	}
	public boolean isPreferredCurrencyLabel() {
		return preferredCurrencyLabel;
	}
	public void setPreferredCurrencyLabel(boolean preferredCurrencyLabel) {
		this.preferredCurrencyLabel = preferredCurrencyLabel;
	}
	public boolean isPreferredCurrencyDrpDwn() {
		return preferredCurrencyDrpDwn;
	}
	public void setPreferredCurrencyDrpDwn(boolean preferredCurrencyDrpDwn) {
		this.preferredCurrencyDrpDwn = preferredCurrencyDrpDwn;
	}
	public boolean isPromotionCodeLabel() {
		return promotionCodeLabel;
	}
	public void setPromotionCodeLabel(boolean promotionCodeLabel) {
		this.promotionCodeLabel = promotionCodeLabel;
	}
	public boolean isPromotionCodeTxtBox() {
		return promotionCodeTxtBox;
	}
	public void setPromotionCodeTxtBox(boolean promotionCodeTxtBox) {
		this.promotionCodeTxtBox = promotionCodeTxtBox;
	}
	public boolean isPreferNonStopFlightLabel() {
		return preferNonStopFlightLabel;
	}
	public void setPreferNonStopFlightLabel(boolean preferNonStopFlightLabel) {
		this.preferNonStopFlightLabel = preferNonStopFlightLabel;
	}
	public boolean isPreferNonStopFlightTxtBox() {
		return preferNonStopFlightChkBox;
	}
	public void setPreferNonStopFlightTxtBox(boolean preferNonStopFlightTxtBox) {
		this.preferNonStopFlightChkBox = preferNonStopFlightTxtBox;
	}
	public boolean isAdditionalOptionTxt() {
		return additionalOptionTxt;
	}
	public void setAdditionalOptionTxt(boolean additionalOptionTxt) {
		this.additionalOptionTxt = additionalOptionTxt;
	}
	public String getAdditionalOptionTxtBeforeClick() {
		return additionalOptionTxtBeforeClick;
	}
	public void setAdditionalOptionTxtBeforeClick(
			String additionalOptionTxtBeforeClick) {
		this.additionalOptionTxtBeforeClick = additionalOptionTxtBeforeClick;
	}
	public String getAdditionalOptionTxtAfterClick() {
		return additionalOptionTxtAfterClick;
	}
	public void setAdditionalOptionTxtAfterClick(
			String additionalOptionTxtAfterClick) {
		this.additionalOptionTxtAfterClick = additionalOptionTxtAfterClick;
	}
	public boolean isSearchButton() {
		return searchButton;
	}
	public void setSearchButton(boolean searchButton) {
		this.searchButton = searchButton;
	}
	
}
