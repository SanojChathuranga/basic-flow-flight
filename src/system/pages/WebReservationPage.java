package system.pages;


import java.io.IOException;
//import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import flow.classes.*;


public class WebReservationPage
{
	private HashMap<String, String>	propertyMap			= new HashMap<String, String>();
	private boolean					done				= false;
	private boolean					isFlightDefaultSelected = false;
	private BookingEnginePage		bookingEnginePage	= new BookingEnginePage();
	

	public boolean isFlightDefaultSelected() {
		return isFlightDefaultSelected;
	}

	public void setFlightDefaultSelected(boolean isFlightDefaultSelected) {
		this.isFlightDefaultSelected = isFlightDefaultSelected;
	}

	public WebReservationPage(HashMap<String, String> Map)
	{
		propertyMap = Map;
	}
	
	public boolean search(WebDriver driver, SearchObject searchObject) throws WebDriverException, IOException
	{
		WebDriverWait	wait				= new WebDriverWait(driver, 15);
		String			Departure			= searchObject.getDepartureDate().trim();
		String			Return				= searchObject.getReturnDate().trim();
		//String			scenarioCommonPath	= "";
		//String			scenarioFailedPath	= "";
		//scenarioCommonPath					= ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		//scenarioFailedPath					= ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		
		try {
			
			driver.switchTo().frame("bec_container_frame");
			try {
				//driver.findElement(By.className(propertyMap.get("HmPg_AdditionalSearchOption_class"))).click();
				driver.findElement(By.className("show_filter")).click();
			} catch (Exception e3) {
				e3.printStackTrace();
			}
			
			try {
				Thread.sleep(5000);
				new Select(driver.findElement(By.id(propertyMap.get("HmPg_ResidenceCountry_DropDwn_id")))).selectByVisibleText(searchObject.getCountry().trim());
			} catch (Exception e) {
				System.out.println("This is a TO Booking dude... No residence country is required...");
			}
			
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id(propertyMap.get("HmPg_From_TxtBox_id"))));	
				Thread.sleep(3000);
				
				driver.findElement(By.id(propertyMap.get("HmPg_From_TxtBox_id"))).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE From Origin drop down.jpg", driver);
				driver.findElement(By.id(propertyMap.get("HmPg_From_TxtBox_id"))).sendKeys(searchObject.getFrom().trim().split("\\|")[0]);
				((JavascriptExecutor)driver).executeScript("document.getElementById('"+propertyMap.get("HmPg_From_Hidden_id")+"').setAttribute('value','"+searchObject.getFrom().trim()+"');");
				
				driver.findElement(By.id(propertyMap.get("HmPg_To_TxtBox_id").trim())).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE To Destination drop down.jpg", driver);
				driver.findElement(By.id(propertyMap.get("HmPg_To_TxtBox_id"))).sendKeys(searchObject.getTo().trim().split("\\|")[0]);
				((JavascriptExecutor)driver).executeScript("document.getElementById('"+propertyMap.get("HmPg_To_Hidden_id")+"').setAttribute('value','"+searchObject.getTo().trim()+"');");
				
			} catch (Exception e) {
				
			}
			
			if (searchObject.getTriptype().equals("Round Trip")) {
				
				//driver.findElement(By.id(propertyMap.get("HmPg_DepartureDate_TxtBox_id"))).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE From Date.jpg", driver);
				((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("HmPg_DepartureDate_TxtBox_id")+"').val('"+Departure+"');");
				//new Select(driver.findElement(By.id(propertyMap.get("HmPg_DepartureTime_DropDwn_id")))).selectByValue(searchObject.getDepartureTime().trim());
				
				//driver.findElement(By.id(propertyMap.get("HmPg_RetrunDate_TxtBox_id"))).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE To Date.jpg", driver);
				((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("HmPg_RetrunDate_TxtBox_id")+"').val('"+Return+"');");
				//new Select(driver.findElement(By.id(propertyMap.get("HmPg_ReturnTime_DropDwn_id")))).selectByValue(searchObject.getReturnTime().trim());
				
			} else if (searchObject.getTriptype().equals("One Way Trip")) {
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id(propertyMap.get("HmPg_OnewayTrip_RadioBtn_id"))));
				((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("HmPg_OnewayTrip_RadioBtn_id")+"').click();");
				try {
					driver.findElement(By.id(propertyMap.get("HmPg_OnewayTrip_RadioBtn_id"))).click();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Thread.sleep(4000);
				try {
					((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("HmPg_OnewayTrip_RadioBtn_id")+"').click();");	
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id(propertyMap.get("HmPg_DepartureDate_TxtBox_id"))));
				//driver.findElement(By.id(propertyMap.get("HmPg_DepartureDate_TxtBox_id"))).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE From Date.jpg", driver);
				((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("HmPg_DepartureDate_TxtBox_id")+"').val('"+searchObject.getDepartureDate().trim()+"');");
				new Select(driver.findElement(By.id(propertyMap.get("HmPg_DepartureTime_DropDwn_id")))).selectByVisibleText(searchObject.getDepartureTime().trim());
			}
			
			//FLEXIBLE FLIGHTS
			if( Boolean.valueOf(searchObject.isFlexible()) ) {
				driver.findElement(By.id(propertyMap.get("HmPg_Flexible_ChkBox_id"))).click();
			}
			
			//=========================PASSENGERS==========================
			//ADULTS
			try {
				driver.findElement(By.id(propertyMap.get("HmPg_AdultCount_DropDwn_id"))).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE Adult drop down.jpg", driver);
				new Select(driver.findElement(By.id(propertyMap.get("HmPg_AdultCount_DropDwn_id")))).selectByValue(searchObject.getAdult());
			} catch (Exception e) {
				
			}
			
			//CHILDREN
			if(!searchObject.getChildren().trim().equals("0")) {
				
				driver.findElement(By.id(propertyMap.get("HmPg_ChildrenCount_DropDwn_id"))).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE Children drop down.jpg", driver);
				new Select(driver.findElement(By.id(propertyMap.get("HmPg_ChildrenCount_DropDwn_id")))).selectByValue(searchObject.getChildren().trim());

				String value = "";
				value = propertyMap.get("HmPg_ChildrenAge_DropDwn_id");
				try {
					for(int i = 1; i<=searchObject.getChildrenAge().size(); i++) {
						value = value.replace("REPLACE", String.valueOf(i));
						new Select(driver.findElement(By.id(value))).selectByValue(searchObject.getChildrenAge().get((i-1)).trim());//Child Loop
					}
				} catch (Exception e) {
					
				}
			}
			
			//INFANT
			if(!searchObject.getInfant().trim().equals("0")) {
				
				driver.findElement(By.id(propertyMap.get("HmPg_InfantCount_DropDwn_id"))).click();
				//Screenshot.takeScreenshot(scenarioCommonPath + "/BE Infant drop down.jpg", driver);
				new Select(driver.findElement(By.id(propertyMap.get("HmPg_InfantCount_DropDwn_id")))).selectByValue(searchObject.getInfant().trim());
			}
			
			//SHOW ADDITIONAL SEARCH OPTIONS
			//driver.findElement(By.xpath(propertyMap.get("HmPg_AdditionalSearchOption_xpath"))).click();
			
			//PREFERRED AIRLINE CODE
			if(!searchObject.getPreferredAirline().trim().equals("NONE")){
				driver.findElement(By.id(propertyMap.get("HmPg_PrefferedAirline_TxtBox_id"))).sendKeys(searchObject.getPreferredAirline().trim());
			}
			
			//CABIN CLASS
			new Select(driver.findElement(By.id(propertyMap.get("HmPg_CabinClass_DropDwn_id")))).selectByValue(searchObject.getCabinClass().trim());
			
			//PREFERRED CURRENCY
			if(!searchObject.getPreferredCurrency().trim().equals("Select Currency")) {
				new Select(driver.findElement(By.id(propertyMap.get("HmPg_PrefferedCurrency_DropDwn_id")))).selectByValue(searchObject.getPreferredCurrency().trim());
			}
			
			//PROMOTION CODE
			/*if (false) {	
				driver.findElement(By.id(propertyMap.get("HmPg_PromotionCode_TxtBox_id"))).sendKeys(searchObject.getPromotionCode().trim());
			}*/
			
			//PREFER NONSTOP
			try {
				if (true) {
					driver.findElement(By.xpath(propertyMap.get("HmPg_PreferNonStop_ChkBox_xpath"))).click();
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			//new Select(driver.findElement(By.id(propertyMap.get("HmPg_ResidenceCountry_DropDwn_id")))).selectByVisibleText(searchObject.getCountry().trim());
			
			if(searchObject.isApplyDiscount())
			{
				try {
					//Screenshot.takeScreenshot(scenarioCommonPath + "/Before Click Show Additional Search.jpg", driver);
					//driver.findElement(By.partialLinkText("Show Additional Search Options  ")).click();
					//Screenshot.takeScreenshot(scenarioCommonPath + "/After Click Show Additional Search.jpg", driver);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id(propertyMap.get("HmPg_PromotionCode_TxtBox_id"))));
					driver.findElement(By.id(propertyMap.get("HmPg_PromotionCode_TxtBox_id"))).sendKeys(propertyMap.get("DiscountCouponNo"));
				} catch (Exception e) {
					System.out.println("Discount No not entered");
				}
				
			}
			
			Thread.sleep(3000);
			try {
				((JavascriptExecutor)driver).executeScript("JavaScript:search('F');");
			} catch (Exception e) {
				try {
					driver.findElement(By.id(propertyMap.get("HmPg_SearchFlights_Btn_id"))).click();
				} catch (Exception e2) {
					
				}				
			}			
			
			done = true;
			
		} catch (Exception e) {
			e.printStackTrace();
			done = false;
		}
		
		return done;
	}	
	
	public BookingEnginePage getBookingEnginePage() {
		return bookingEnginePage;
	}

	public void setBookingEnginePage(BookingEnginePage bookingEnginePage) {
		this.bookingEnginePage = bookingEnginePage;
	}

	public boolean isElementPresent(WebDriver driver, By by)
	{
		try {
			driver.findElement(by);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public boolean isElementPresent(WebDriver driver, WebElement ele)
	{
		boolean j = false;
		try {
			if(ele.isDisplayed())
			{
				j = true;
			}
		} catch (Exception e) {
			return false;
		}
		
		return j;
	}
	
}
