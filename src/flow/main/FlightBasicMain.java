package flow.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import flow.classes.*;
import flow.enumtypes.*;
import flow.utilities.*;

public class FlightBasicMain {

	HashMap<String, String> propertyMap = new HashMap<String, String>();
	//String propertyPath = "TestData/PropertiesWEBBasic.properties";
	String propertyPath = "TestData/PropertiesWEB_ProdBasic.properties";
	//String propertyPath = "TestData/PropertiesWEBET.properties";
	StringBuffer ReportPrinter = null;
	SupportMethods support = null;
	WebDriver driver = null;
	boolean login = false;
	ArrayList<Map<Integer, String>> XLtestData = new ArrayList<Map<Integer, String>>();
	int m = 1;
	int Reportcount = 1;
	int count = 1;

	@Before
	public void born() {
		ReportPrinter = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Flight Reservation - Web (" + sdf.format(Calendar.getInstance().getTime()) + ")</p></div>");
		ReportPrinter.append("<body>");

		try {
			propertyMap = ReadProperties.readpropeties(propertyPath);
			support = new SupportMethods(propertyMap);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ExcelReader Reader = new ExcelReader();
		XLtestData = Reader.init(propertyMap.get("XLPath"));
	}

	@Test
	public void live() {

		String scenarioCommonPath = "";
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		Web web = new Web();
		CC cc = new CC();

		ArrayList<SearchObject> list = getSearchObjList();

		AirConfiguration airconfig = new AirConfiguration(propertyMap);
		ArrayList<AirConfig> configlist = airconfig.getConfigurationList();

		Iterator<SearchObject> listiter = list.iterator();

		while (listiter.hasNext()) {
			SearchObject searchObject = listiter.next();
			if (searchObject.getExcecuteStatus().equalsIgnoreCase("YES")) {
				ScreenshotPathSingleton.getInstance().setScenarioPaths(searchObject.getScenario());
				try {
					driver = support.initalizeDriver();
					login = support.login(driver, propertyMap.get("portal.username"), propertyMap.get("portal.password"));
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (login) {
					ReportPrinter.append("<span><center><p class='Hedding0'>TEST CASE " + m + " STARTED</p></center></span>");
					ReportPrinter.append("<br>");
					Repository.setAll();
					Screenshot.takeScreenshot(scenarioCommonPath + "/Login Passed.jpg", driver);

					try {

						AirConfig configuration = new AirConfig();
						try {
							AirConfig configuration1 = configlist.get(m - 1);
							driver = airconfig.setConfiguration(driver, configuration1);
							configuration = configuration1;
						} catch (Exception e) {
							e.printStackTrace();
						}

						String type = searchObject.getSearchType().trim();

						if (type.equals(SearchType.Call_Center.toString())) {

							cc.go(driver, searchObject, configuration, propertyMap, ReportPrinter, Reportcount);

						} else if (type.equals(SearchType.Web.toString())) {

							web.go(driver, searchObject, configuration, propertyMap, ReportPrinter, Reportcount);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					Screenshot.takeScreenshot(scenarioFailedPath + "/Login Failed.jpg", driver);
				}
				m += 1;
			}

			try {
				//driver.quit();
			} catch (Exception e) {
				e.printStackTrace();
			}
			count += 1;
		}
	}

	@After
	public void die() {

		ReportPrinter.append("</body></html>");
		BufferedWriter bwr;
		try {
			bwr = new BufferedWriter(new FileWriter(new File("Reports/Flight Web Reservation.html")));
			bwr.write(ReportPrinter.toString());
			bwr.flush();
			bwr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//driver.quit();

	}

	public ArrayList<SearchObject> getSearchObjList() {
		ArrayList<SearchObject> SearchList = new ArrayList<SearchObject>();

		for (int y = 0; y < 1; y++) {
			Map<Integer, String> Sheet = XLtestData.get(y);

			for (int x = 0; x < Sheet.size(); x++) {
				String[] testData = Sheet.get(x).split(",");
				SearchObject SearchObj = new SearchObject();
				SearchObj.setSearchType(testData[0].trim());
				SearchObj.setSellingCurrency(testData[1].trim());
				SearchObj.setCountry(testData[2].trim());
				SearchObj.setTriptype(testData[3].trim());
				SearchObj.setFrom(testData[4].trim());
				SearchObj.setTo(testData[5].trim());
				SearchObj.setDepartureDate(testData[6].trim());
				SearchObj.setDepartureTime(testData[7].trim());
				SearchObj.setReturnDate(testData[8].trim());
				SearchObj.setReturnTime(testData[9].trim());
				SearchObj.setFlexible(Boolean.parseBoolean(testData[10].toLowerCase().trim()));
				SearchObj.setAdult(testData[11].trim());
				SearchObj.setChildren(testData[12].trim());

				ArrayList<String> childrenage = new ArrayList<String>();
				String[] age = testData[13].trim().split("/");

				for (int u = 0; u < age.length; u++) {
					childrenage.add(age[u].trim());
				}

				SearchObj.setChildrenAge(childrenage);
				SearchObj.setInfant(testData[14].trim());
				SearchObj.setCabinClass(testData[15].trim());
				SearchObj.setPreferredCurrency(testData[16].trim());
				SearchObj.setPreferredAirline(testData[17].trim());
				SearchObj.setNonStop(Boolean.parseBoolean(testData[18].toLowerCase().trim()));

				SearchObj.setProfitType(testData[19].toLowerCase().trim());
				SearchObj.setProfit(Double.parseDouble(testData[20].trim()));

				SearchObj.setSelectingFlight(testData[21].trim());

				SearchObj.setBookingFeeType(testData[22].trim());
				SearchObj.setBookingFee(Double.parseDouble(testData[23].trim()));

				SearchObj.setExcecuteStatus(testData[24].trim());
				SearchObj.setPaymentMode(testData[25].trim());
				SearchObj.setCancellationStatus(testData[26].trim());
				SearchObj.setSupplierPayablePayStatus(testData[27].trim());
				SearchObj.setRemovecartStatus(Boolean.parseBoolean(testData[29].trim()));
				SearchObj.setQuotation(Boolean.parseBoolean(testData[30].trim()));
				SearchObj.setSearchAgain(Boolean.parseBoolean(testData[31].trim()));
				SearchObj.setValidateFilters(Boolean.parseBoolean(testData[32].trim()));
				SearchObj.setTOBooking(testData[33].trim());
				SearchObj.setApplyDiscount(Boolean.parseBoolean(testData[34].trim()));
				SearchObj.setApplyDiscountAtPayPg(Boolean.parseBoolean(testData[35].trim()));

				if (testData[36].trim().equalsIgnoreCase("NetCash")) {
					SearchObj.setToType(TO.NetCash);
				} else if (testData[36].trim().equalsIgnoreCase("NetCreditLpoY")) {
					SearchObj.setToType(TO.NetCreditLpoY);
				} else if (testData[36].trim().equalsIgnoreCase("NetCreditLpoN")) {
					SearchObj.setToType(TO.NetCreditLpoN);
				} else if (testData[36].trim().equalsIgnoreCase("CommCash")) {
					SearchObj.setToType(TO.CommCash);
				} else if (testData[36].trim().equalsIgnoreCase("CommCreditLpoY")) {
					SearchObj.setToType(TO.CommCreditLpoY);
				} else if (testData[36].trim().equalsIgnoreCase("CommCreditLpoN")) {
					SearchObj.setToType(TO.CommCreditLpoN);
				}
				SearchObj.setScenario(testData[37].trim());

				SearchList.add(SearchObj);
			}
		}

		return SearchList;
	}

}
