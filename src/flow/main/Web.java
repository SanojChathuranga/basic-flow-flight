package flow.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import system.pages.*;
import flow.utilities.*;
import flow.classes.*;
import flow.enumtypes.*;

public class Web {

	ArrayList<UIFlightItinerary> WebResultlist = new ArrayList<UIFlightItinerary>();
	CartFlight cart = null;
	HashMap<String, String> PropertyMap = new HashMap<String, String>();
	Map<String, String> CurrencyMap = new HashMap<String, String>();
	boolean done = false;
	boolean results = false;
	boolean eticketIssued = false;
	
	public void go(WebDriver driver, SearchObject searchObject, AirConfig configuration, HashMap<String, String> propertyMap, StringBuffer ReportPrinter, int Reportcount) {

		PropertyMap = propertyMap;
		WebReservationPage webReservationPage = new WebReservationPage(propertyMap);
		//WebResultsPage webResultsPage = new WebResultsPage(propertyMap);
		WebPaymentPage webPaymentPage = new WebPaymentPage(propertyMap);
		//WebConfirmationPage webConfirmationPage = new WebConfirmationPage(propertyMap);

		try {
			Repository.setAll();
			// READ EXCHANGE RATE TABLE TO HASH MAP
			ExchangeRateUpdater rates = new ExchangeRateUpdater();
			try {
				CurrencyMap = rates.getExchangeRates(PropertyMap, driver);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				if (searchObject.getTOBooking().equalsIgnoreCase("YES")) {
					try {
						// CHECK THE LOGING TYPE
						boolean log = false;
						SupportMethods support = new SupportMethods(propertyMap);
						String userName = "";
						String password = "";
						try {
							if (searchObject.getToType() == TO.NetCash) {
								userName = propertyMap.get("NetCash");
								password = propertyMap.get("NetCashPass");
							} else if (searchObject.getToType() == TO.NetCreditLpoN) {
								userName = propertyMap.get("NetCreditLpoN");
								password = propertyMap.get("NetCreditLpoNPass");
							} else if (searchObject.getToType() == TO.NetCreditLpoY) {
								userName = propertyMap.get("NetCreditLpoY");
								password = propertyMap.get("NetCreditLpoYPass");
							} else if (searchObject.getToType() == TO.CommCash) {
								userName = propertyMap.get("CommCash");
								password = propertyMap.get("CommCashPass");
							} else if (searchObject.getToType() == TO.CommCreditLpoN) {
								userName = propertyMap.get("CommCreditLpoN");
								password = propertyMap.get("CommCreditLpoNPass");
							} else if (searchObject.getToType() == TO.CommCreditLpoY) {
								userName = propertyMap.get("CommCreditLpoY");
								password = propertyMap.get("CommCreditLpoYPass");
							}

							log = support.login(driver, userName, password);
						} catch (Exception e) {

						}

						// LOGIN SUCCESS VALIDATION
						ReportPrinter.append("<span><center><p class='Hedding0'>Check TO Login</p></center></span>");
						ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
						ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>TO Login</td>" + "<td>If Login - true, If fail - false</td>");
						if (log) {
							ReportPrinter.append("<td>" + log + "</td>" + "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
						} else {
							ReportPrinter.append("<td>" + log + "</td>" + "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
						}
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");

						driver.switchTo().defaultContent();

						// CLICK RESERVAION LINK
						if (true) {
							driver.findElement(By.partialLinkText("Make Reservations")).click();
							@SuppressWarnings("unused")
							boolean done = webReservationPage.search(driver, searchObject);
						}
					} catch (Exception e) {
						System.out.println("Smthing wrong with the TO LOGIN dude...!!!!");
					}
				} else {
					// DIRECT USER ~ NOT TO
					driver.get(PropertyMap.get("PortalWEB.Url"));
					@SuppressWarnings("unused")
					boolean done = webReservationPage.search(driver, searchObject);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			SupportMethods su = new SupportMethods(propertyMap);
			PopupMessage popup1 = new PopupMessage();

			driver.switchTo().defaultContent();
			WebDriverWait wait = new WebDriverWait(driver, 25);

			// GET THE TRACER FROM THE RESULTS PAGE
			String tracer = "null";
			try {
				results = getTracer(driver, searchObject, tracer);
				if (!results) {
					popup1 = su.popupHandler(driver);
				}

				// VALIDATION OF RESULTS AVAILABILITY AND THE NOTIFICATION POPUP FOR RESULTS UNAVAILABILITY
				ReportPrinter.append("<span><center><p class='Hedding0'>Check No Results Notification Functionality</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Booking Engine Search</td>" + "<td>If Results Available In Results Page - True and Notification - False</td>");
				if (!popup1.getTitle().equals("") && results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				} else if (popup1.getTitle().equals("") && results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				} else if (!popup1.getTitle().equals("") && !results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				} else if (popup1.getTitle().equals("") && !results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");

				// RESULTS AVAILABILITY TRUE OR FALSE
				ReportPrinter.append("<span><center><p class='Hedding0'>Check Results Availability</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Booking Engine Search</td>" + "<td>If Results Available In Results Page - True</td>");
				if (results) {
					ReportPrinter.append("<td>" + results + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				} else {
					ReportPrinter.append("<td>" + results + "</td>" + "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");

			} catch (Exception e) {
				e.printStackTrace();
			}

			// VALIDATION OF NO RESULTS NOTIFICATION
			if (!results) {
				ReportPrinter.append("<span><center><p class='Hedding0'>No Results Notification Validation</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check proper implementation of No Results Notification</td>" + "<td>If Results Not Available In Results Page, Notification should be displayed</td>");

				PopupMessage popupErrorFareRes = new PopupMessage();
				try {
					popupErrorFareRes = su.popupHandler(driver);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (!popupErrorFareRes.getMessage().equals("")) {
					ReportPrinter.append("<td>" + popupErrorFareRes.getMessage() + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				} else {
					ReportPrinter.append("<td>" + popupErrorFareRes.getMessage() + "</td>" + "<td>Fail</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
			}// Results Not Available

			if (results) {

				int flights = 0;
				// FLIGHTS AVAILABLE
				try {
					flights = driver.findElements(By.className(propertyMap.get("ResultPg_FlightItinerary_article_class").trim())).size();
				} catch (Exception e) {
					e.printStackTrace();
				}

				// ADD TO CART
				String selection = searchObject.getSelectingFlight();
				int selectFlight = Integer.parseInt(selection);
				PopupMessage popupaddtocart = new PopupMessage();
				try {
					Thread.sleep(3000);
					popupaddtocart = su.addToCart(driver, flights, selectFlight);
					try {
						if (driver.findElement(By.className("popup-body")).isDisplayed()) {
							
							ArrayList<WebElement> ele = new ArrayList<WebElement>(driver.findElements(By.className("close-popup")));
							ele.get(0).click();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					// PRICE VALIDATION WHILE ADD TO CART
					ReportPrinter.append("<span><center><p class='Hedding0'>Price Validation While Add To Cart</p></center></span>");
					ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
					ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>If Prices have changed actual must give an alert message</td>" + "<td>Error/Notification</td>");

					if (popupaddtocart.getMessagetype().equals("Error")) {
						ReportPrinter.append("<td>Message : " + popupaddtocart.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
						Reportcount++;

						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");

					} else if (popupaddtocart.getMessagetype().equals("Warning") || popupaddtocart.getMessagetype().equals("")) {
						ReportPrinter.append("<td>Message : " + popupaddtocart.getMessage() + "</td>" + "<td class='Passed'>Fail</td></tr>");
						Reportcount++;

						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");

						try {
							((JavascriptExecutor) driver).executeScript("JavaScript:loadPaymentPage();");
						} catch (Exception e) {
							e.printStackTrace();
						}

						// ERROR VALIDATION WHEN CHECKOUT FROM CART
						ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
						ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
						ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check blocker errors that prevents checkout</td>" + "<td>If error occurred - Error Message should be displayed</td>");

						PopupMessage popup2 = new PopupMessage();

						// HANDLE DISCOUNT APPLICATION MESSAGE ALERT
						/*
						 * try { if (!driver.findElement(By.id("status_F_AIR1")).isDisplayed() || !driver.findElement(By.id("customername")).isDisplayed() ||
						 * !driver.findElement(By.id("dialog-alert-message-WJ_19")).isDisplayed()) { if (driver.findElement(By.id("dialog-alert-message-WJ_19")).isDisplayed()) { String alert =
						 * driver.findElement(By.id("dialog-alert-message-WJ_19")).getText(); if (!alert.contains("The discount has been applied according to the conditions")) { popup2 =
						 * su.popupHandler(driver); } } } } catch (Exception e) { e.printStackTrace(); }
						 */

						if (!popup2.getMessage().equals("")) {
							ReportPrinter.append("<td>Popup error message : " + popup2.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
						} else {
							ReportPrinter.append("<td>No Errors</td>" + "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");

							// FILLING CUSTOMER DETAILS FOR THE FILLING OBJECT
							FillReservationDetails fill = new FillReservationDetails();
							ReservationInfo fillingObject = new ReservationInfo();
							try {
								fillingObject = fill.Fill_Reservation_details(searchObject);
								done = webPaymentPage.setPaymentPage(driver, fillingObject, searchObject, PropertyMap, CurrencyMap);
							} catch (Exception e) {
								e.printStackTrace();
							}

							if (!searchObject.isQuotation()) {

								WebDriverWait wait2 = new WebDriverWait(driver, 25);

								try {
									((JavascriptExecutor) driver).executeScript("javaScript:hideandSubmitThirdParty('true');");

								} catch (Exception e) {
									try {
										((JavascriptExecutor) driver).executeScript("javaScript:hideandSubmitThirdParty('false');");
									} catch (Exception e2) {
										e2.printStackTrace();
									}
								}
								try {
									driver.findElement(By.id("divConfirm")).findElement(By.className("complete-btn")).click();
								} catch (Exception e) {
									e.printStackTrace();
								}

								try {
									driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);

									if (searchObject.getPaymentMode().equalsIgnoreCase("Pay Online")) {
										driver.switchTo().frame("paygatewayFrame");
										wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
										((JavascriptExecutor) driver).executeScript("$('#cardnumberpart1').val('" + PropertyMap.get("CardNo1") + "');");
										((JavascriptExecutor) driver).executeScript("$('#cardnumberpart2').val('" + PropertyMap.get("CardNo2") + "');");
										((JavascriptExecutor) driver).executeScript("$('#cardnumberpart3').val('" + PropertyMap.get("CardNo3") + "');");
										((JavascriptExecutor) driver).executeScript("$('#cardnumberpart4').val('" + PropertyMap.get("CardNo4") + "');");
										new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(PropertyMap.get("CardMonth"));
										new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(PropertyMap.get("CardYear"));
										driver.findElement(By.id("cardholdername")).sendKeys(PropertyMap.get("CardHolder"));
										((JavascriptExecutor) driver).executeScript("$('#cv2').val(" + PropertyMap.get("CardHolderSSN") + ");");

										Thread.sleep(3000);
										driver.findElement(By.className("proceed_btn")).click();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								try {
									wait.until(ExpectedConditions.visibilityOf(driver.findElement(By
											.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))));
									if (driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).isDisplayed()) {
										driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");
										// ((JavascriptExecutor) driver).executeScript("submitForm()")
										driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

								try {
									done = false;
									String resNO = "";
									wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id(propertyMap.get("ConfPg_BookingReference_Lbl_id").trim()))));
									resNO = driver.findElement(By.id(propertyMap.get("ConfPg_BookingReference_Lbl_id").trim())).getText().trim();

									if (!resNO.equals("")) done = true;
									ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation(Web)</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									CommonValidator.elementAvailable(done, "Confirmation page displayed", ReportPrinter, Reportcount);
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");

								} catch (Exception e) {
									e.printStackTrace();
									ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation(Web)</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									CommonValidator.elementAvailable(done, "Confirmation page displayed", ReportPrinter, Reportcount);
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
								}
							}// NOT QUOTATION
							if(searchObject.isQuotation()){
								CallCenter CCRun = new CallCenter();
								CCRun.setPropertymap(PropertyMap);
								QuotationPage quotation = new QuotationPage(PropertyMap);
								try {
									done = quotation.setQuotation(driver, searchObject);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								try {
									QuotationBooking bookQuotation = new QuotationBooking(PropertyMap);
									boolean retreival = false;
									retreival = bookQuotation.loadQuotation(driver, searchObject, quotation);
									try {
										boolean x = false;
										try {
											x = driver.findElement(By.id("loadpayment")).isDisplayed();
										} catch (Exception e) {
											System.out.println("loadpayment element displayed");
										}

										if (x) {
											try {
												wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loadpayment")));
												((JavascriptExecutor) driver).executeScript("JavaScript:loadPaymentPage();");
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									} catch (Exception e) {
										System.out.println("quotation load fails");
									}
									ReportPrinter.append("<span><center><p class='Hedding0'>Quotation Retrieval</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check quotation retrieval</td>" + "<td>If retrieval pass - true , if fails - false</td>");
									if (retreival) {
										ReportPrinter.append("<td>" + retreival + "</td>" + "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									} else {
										ReportPrinter.append("<td>" + retreival + "</td>" + "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									
									ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check blocker errors that prevents checkout</td>" + "<td>If error occurred - Error Message should be displayed</td>");

									PopupMessage popupcc = new PopupMessage();
									if (!popupcc.getMessage().equals("")) {
										ReportPrinter.append("<td>Popup error message : " + popupcc.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									} else {
										ReportPrinter.append("<td>No Errors</td>" + "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									
									try {
										FillReservationDetails fillcc = new FillReservationDetails();
										@SuppressWarnings("unused")
										ReservationInfo fillingObjectcc = new ReservationInfo();
										fillingObjectcc = fillcc.Fill_Reservation_details(searchObject);

										driver = CCRun.Fill_Reservation_Details(driver, fillingObject, searchObject);

									} catch (Exception e) {
										e.printStackTrace();
									}
									
									ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check blocker errors that prevents checkout</td>" + "<td>If error occurred - Error Message should be displayed</td>");

									PopupMessage popup3 = new PopupMessage();
									try {
										if (!driver.findElement(By.id("pre_res_dlgWJ_13")).isDisplayed()) {
											popup2 = su.popupHandler(driver);
										}

									} catch (Exception e) {

									}

									if (!popup3.getMessage().equals("")) {
										ReportPrinter.append("<td>Popup error message : " + popup3.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									} else {
										ReportPrinter.append("<td>No Errors</td>" + "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
										
										if (searchObject.getPaymentMode().equalsIgnoreCase("Pay Online")) {
											try {
												driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
												driver.switchTo().frame("paygatewayFrame");
												WebDriverWait wait2 = new WebDriverWait(driver, 3);
												wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
												((JavascriptExecutor) driver).executeScript("$('#cardnumberpart1').val('" + PropertyMap.get("CardNo1") + "');");
												((JavascriptExecutor) driver).executeScript("$('#cardnumberpart2').val('" + PropertyMap.get("CardNo2") + "');");
												((JavascriptExecutor) driver).executeScript("$('#cardnumberpart3').val('" + PropertyMap.get("CardNo3") + "');");
												((JavascriptExecutor) driver).executeScript("$('#cardnumberpart4').val('" + PropertyMap.get("CardNo4") + "');");

												new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(PropertyMap.get("CardMonth"));
												new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(PropertyMap.get("CardYear"));
												driver.findElement(By.id("cardholdername")).sendKeys(PropertyMap.get("CardHolder"));
												((JavascriptExecutor) driver).executeScript("$('#cv2').val(" + PropertyMap.get("CardHolderSSN") + ");");

												try {
													driver.findElement(By.className("proceed_btn")).click();
												} catch (Exception e) {
													e.printStackTrace();
													try {
														((JavascriptExecutor) driver).executeScript("javascript:submit_page()");
													} catch (Exception e2) {
														e.printStackTrace();
													}
												}

												try {
													driver.findElement(By.id("divConfirm")).findElement(By.className("complete-btn")).click();
												} catch (Exception e) {

												}

												driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

												try {
													wait.until(ExpectedConditions.visibilityOf(driver.findElement(By
															.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))));
													if (driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))
															.isDisplayed()) {
														driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))
																.sendKeys("1234");

														driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();
													}
												} catch (Exception e) {
													e.printStackTrace();
												}

											} catch (Exception e) {

											}
										}
										
										PopupMessage popupatCreditPay = new PopupMessage();
										popupatCreditPay = su.popupHandler(driver);

										ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For Unexpected Blocker Issues while Payment</p></center></span>");
										ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
										ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Test for payment gateway issues</td>" + "<td>If error occured - Error Message should be displayed</td>");

										if (!popupatCreditPay.getMessage().equals("") && !popupatCreditPay.getMessage().contains("Unable to create the E-Ticket")) {
											ReportPrinter.append("<td>Popup error message : " + popupatCreditPay.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										} else {

											if (popupatCreditPay.getMessage().contains("Unable to create the E-Ticket")) {
												eticketIssued = false;
											} else {
												eticketIssued = true;
											}

											ReportPrinter.append("<td>No Payment gateway errors</td>" + "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");	
										}
									}// PASS NO ERRORS CHECKOUT
									
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}// END CHECK OUT SUCCESS
					}// END ADD TO CART PASS
				} catch (Exception e) {
					e.printStackTrace();
				}
			}// Results Available

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean getTracer(WebDriver driver, SearchObject searchObject, String tracer) {
		boolean results = false;
		WebDriverWait wait = new WebDriverWait(driver, 25);
		try {
			if (searchObject.isSearchAgain()) {
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())));
					tracer = driver.findElement(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())).getAttribute("value");
					results = true;
				} catch (Exception e) {
					// e.printStackTrace();
				}

			} else {
				try {
					try {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())));
						tracer = driver.findElement(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())).getAttribute("value");
						results = true;
					} catch (Exception e) {
						// e.printStackTrace();
					}
					if (tracer == null) {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PropertyMap.get("ResultPg_Tracer_id_2").trim())));
						tracer = driver.findElement(By.id(PropertyMap.get("ResultPg_Tracer_id_2").trim())).getAttribute("value");
						results = true;
					}

				} catch (Exception e) {
					// e.printStackTrace();
				}

			}

			searchObject.setTracer(tracer);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}

}
