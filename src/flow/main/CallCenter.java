package flow.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import flow.utilities.*;

import flow.classes.*;
import system.pages.WebConfirmationPage;

public class CallCenter {
	
	String 							PropfilePath 	= "Properties.properties";
	String 							XLPath 			= "";
	ArrayList<Map<Integer, String>> CCXLtestData	= null;
	HashMap<String, String> 		Propertymap 	= new HashMap<String, String>();
	ArrayList<SearchObject> 		CCSearchList 	= null;
	WebDriver 						driver 			= null;
	String 							tracer 			= "";
	String							direction		= "";
	String							Discount		= "";
	boolean							discountApplied	= false;
	String							discountText	= "";
	
	
	
	
	public HashMap<String, String> getPropertymap() {
		return Propertymap;
	}

	public void setPropertymap(HashMap<String, String> propertymap) {
		Propertymap = propertymap;
	}

	public String getDiscountText() {
		return discountText;
	}

	public void setDiscountText(String discountText) {
		this.discountText = discountText;
	}

	public String getDiscount() {
		return Discount;
	}

	public void setDiscount(String discount) {
		Discount = discount;
	}

	public boolean isDiscountApplied() {
		return discountApplied;
	}

	public void setDiscountApplied(boolean discountApplied) {
		this.discountApplied = discountApplied;
	}
	
	public UIPaymentPage getPaymentPage(WebDriver driver, XMLPriceItinerary XMLSelectedFlight, SearchObject searchObject ) throws WebDriverException, IOException
	{
		UIPaymentPage uipaymentpage = new UIPaymentPage();
		
		boolean twoway = false;
		direction = searchObject.getTriptype();
		if(direction.equals("Round Trip"))
		{
			twoway = true;
		}
		
		try
		{
			WebElement alloutbound = driver.findElement(By.id("table_flight_outbound_info"));
			String outbounddetails = alloutbound.getText();
			
			String[] outboundArray = outbounddetails.split("\\n");
			ArrayList<String> sets = new ArrayList<String>();
			ArrayList<Flight> outflightlist = new ArrayList<Flight>();
			
			for(int y=0; y<outboundArray.length; y++)
			{
				if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
				{
					String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
					sets.add(h);
				}
			}
			
			Outbound outbound = new Outbound();
			for(int y=0; y<sets.size(); y++)
			{
				Flight flight = new Flight();
				String Sflight = sets.get(y);
				String depart = Sflight.split("#")[0];
				String arrive = Sflight.split("#")[1];
				
				String[] departArray = depart.split(" ");
				
				String Ddate = "-";
				Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
				flight.setDepartureDate(Ddate);
				String Dtime = "-";
				Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
				flight.setDepartureTime(Dtime);
				
				flight.setFlightNo(depart.split("[()]")[3]);
				flight.setDepartureLocationCode(depart.split("[()]")[1]);
				
				String[] arriveArray = arrive.split(" ");
				String Adate = "-";
				Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
				String Atime = "-";
				Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
				flight.setArrivalDate(Adate);
				flight.setArrivalTime(Atime);
				
				flight.setArrivalLocationCode(arrive.split("[()]")[1]);
				
				outflightlist.add(flight);	
			}
			outbound.setOutBflightlist(outflightlist);
			uipaymentpage.setOutbound(outbound);
			
			WebElement allinbound = null;
			
			if(twoway)
			{
				Inbound inbound = new Inbound();
				allinbound = driver.findElement(By.id("table_flight_inbound_info"));
				String inbounddetails = allinbound.getText();
				
				String[] inboundArray = inbounddetails.split("\\n");
				ArrayList<String> insets = new ArrayList<String>();
				ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
				
				for(int y=0; y<inboundArray.length; y++)
				{
					if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
					{
						String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
						insets.add(h);
					}
				}
				
				for(int y=0; y<insets.size(); y++)
				{
					Flight flight = new Flight();
					String Sflight = insets.get(y);
					String depart = Sflight.split("#")[0];
					String arrive = Sflight.split("#")[1];
					
					String[] departArray = depart.split(" ");
					
					String Ddate = "-";
					Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
					flight.setDepartureDate(Ddate);
					String Dtime = "-";
					Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
					flight.setDepartureTime(Dtime);
					
					flight.setFlightNo(depart.split("[()]")[3]);
					flight.setDepartureLocationCode(depart.split("[()]")[1]);
					
					String[] arriveArray = arrive.split(" ");
					String Adate = "-";
					Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
					String Atime = "-";
					Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
					flight.setArrivalDate(Adate);
					flight.setArrivalTime(Atime);
					
					flight.setArrivalLocationCode(arrive.split("[()]")[1]);
					
					inflightlist.add(flight);
				}
				
				inbound.setInBflightlist(inflightlist);
				uipaymentpage.setInbound(inbound);
			}
			
			WebElement ratetable = driver.findElement(By.id("booking_rate_details_table"));
			ArrayList<WebElement> rateTRtags = new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
			
			int passengercount = rateTRtags.size() - 6;
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();
			for(int h=2; h<(2+passengercount); h++)
			{	
				RatesperPassenger rate = new RatesperPassenger();
				ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
				
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
				{
					rate.setPassengertype("ADT");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
				{
					rate.setPassengertype("CHD");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
				{
					rate.setPassengertype("INF");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				
				ratelist.add(rate);
			}
			uipaymentpage.setRatelist(ratelist);
			
			
			int forward = 2 + passengercount;
			
			UIPriceInfo priceinfo = new UIPriceInfo();
			
			priceinfo.setTotalbefore(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
			priceinfo.setTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
			priceinfo.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
			priceinfo.setTotalCost(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
			
			WebElement totfareTable = driver.findElement(By.id("total_transfer_info_table"));
			String[] totfareArray = totfareTable.getText().split("\\n");
			priceinfo.setCurrencycode(totfareArray[0].split("[()]")[1]);
			priceinfo.setSubtotal(totfareArray[1].split(":")[1].trim());
			priceinfo.setTaxplusother(totfareArray[3]);
			priceinfo.setTotal(totfareArray[5]);
			priceinfo.setAmountprocess(totfareArray[7]);
			priceinfo.setAmountprocessAirline(totfareArray[8].split(" ")[5]);
			
			//Discount UI
			try {
				if(searchObject.isApplyDiscount())
				{
					String discount = "0";
					discount = driver.findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText();
					if(discount.contains("-"))
					{
						discount = discount.trim().replace("-", "");
					}
					priceinfo.setDiscount(discount);
				}
				
			} catch (Exception e) {
				
			}
			
			uipaymentpage.setUisummarypay(priceinfo);
			
			if(searchObject.getPaymentMode().equalsIgnoreCase("Pay Online"))
			{
				UICreditCardPayInfo cardinfo = new UICreditCardPayInfo();
				
				String totredbookingvalue = driver.findElement(By.id("totchgpgamt")).getText();
				String totredCurrencyCode = driver.findElement(By.id("totchgpgcurrency")).getText().split("[()]")[1];
				String gross = driver.findElement(By.id("pkg_depositval")).getText();
				String tax = driver.findElement(By.id("totaltaxpgcurrid")).getText();
				String total = driver.findElement(By.id("totalchargeamtpgcurrid")).getText();
				String amountprocessnw = driver.findElement(By.id("totalpaynowamountpgcurrid")).getText();
				String amountprocessbyAirLine = driver.findElement(By.id("totalamountbyairlinepgcurrid")).getText();
				
				cardinfo.setTotalpackage_bookVal(totredbookingvalue);
				cardinfo.setTotalpackage_bookVal_CurrCode(totredCurrencyCode);
				cardinfo.setSubtotal(gross);
				cardinfo.setTaxandfees(tax);
				cardinfo.setTotal(total);
				cardinfo.setAmountprocess(amountprocessnw);
				cardinfo.setAmountprocessAirline(amountprocessbyAirLine);
				
				uipaymentpage.setCreditcardpay(cardinfo);
			}
			
			WebElement				cancellation		= driver.findElement(By.id("table_cancellation_policies"));
			ArrayList<WebElement>	cancellationTRtags	= new ArrayList<WebElement>(cancellation.findElements(By.tagName("tr")));
			String					flightdeadline		= "";
			try
			{
				flightdeadline = cancellationTRtags.get(1).getText();
				flightdeadline = flightdeadline.split(": ")[1].trim();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				flightdeadline = "";
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
			}
			uipaymentpage.setCancellationdate(flightdeadline);
			
			String packagecancel = "";
			try
			{
				packagecancel =	cancellationTRtags.get((cancellationTRtags.size() - 3)).getText();
				packagecancel =	packagecancel.split(":")[1];
			}
			catch(Exception e)
			{
				e.printStackTrace();
				packagecancel = "";
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
			}
			uipaymentpage.setPackageDeadline(packagecancel);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		return uipaymentpage;
	}

	public UIConfirmationPage getConfirmationPage(WebDriver driver, XMLPriceItinerary XMLSelectedFlight) throws WebDriverException, IOException
	{
		UIConfirmationPage uiconfirmationpage = new UIConfirmationPage();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		
		boolean twoway = false;
		if(XMLSelectedFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
		}
		
		try
		{
			String ReservationNo = driver.findElement(By.id("p_flight_booking_reservation_no_1")).getText().split(":")[1];
			String suppConfirmationNo = driver.findElement(By.id("p_flight_booking_supplier_confirmation_no_1")).getText().split(":")[1];
			
			uiconfirmationpage.setReservationNo(ReservationNo.trim());
			uiconfirmationpage.setAvailable(true);
			uiconfirmationpage.setSupplierConfirmationNo(suppConfirmationNo.trim());
			
			WebElement alloutbound = driver.findElement(By.id("table_flight_outbound_info"));
			String outbounddetails = alloutbound.getText();
			//System.out.println(alloutbound.getText());
			
			String[] outboundArray = outbounddetails.split("\\n");
			ArrayList<String> sets = new ArrayList<String>();
			ArrayList<Flight> outflightlist = new ArrayList<Flight>(); 
			
			for(int y=0; y<outboundArray.length; y++)
			{
				if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
				{
					String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
					sets.add(h);
				}
			}
			
			Outbound outbound = new Outbound();
			for(int y=0; y<sets.size(); y++)
			{
				Flight flight = new Flight();
				String Sflight = sets.get(y);
				String depart = Sflight.split("#")[0];
				String arrive = Sflight.split("#")[1];
				
				String[] departArray = depart.split(" ");
				
				//System.out.println(departArray[1]);
				String Ddate = "-";
				Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
				flight.setDepartureDate(Ddate);
				String Dtime = "-";
				Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
				flight.setDepartureTime(Dtime);
				
				flight.setFlightNo(depart.split("[()]")[3]);
				flight.setDepartureLocationCode(depart.split("[()]")[1]);
				
				String[] arriveArray = arrive.split(" ");
				String Adate = "-";
				Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
				String Atime = "-";
				Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
				flight.setArrivalDate(Adate);
				flight.setArrivalTime(Atime);
				
				flight.setArrivalLocationCode(arrive.split("[()]")[1]);
				//System.out.println(flight.getArrivalLocationCode());
				
				outflightlist.add(flight);
				
			}
			outbound.setOutBflightlist(outflightlist);
			uiconfirmationpage.setOutbound(outbound);
			
			//Inbound
			WebElement allinbound = null;
			
			if(twoway)
			{
				Inbound inbound = new Inbound();
				allinbound = driver.findElement(By.id("table_flight_inbound_info"));
				//System.out.println();
				String inbounddetails = allinbound.getText();
				//System.out.println(inbounddetails);
				
				String[] inboundArray = inbounddetails.split("\\n");
				ArrayList<String> insets = new ArrayList<String>();
				ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
				
				for(int y=0; y<inboundArray.length; y++)
				{
					if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
					{
						String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
						insets.add(h);
					}
				}
				
				for(int y=0; y<insets.size(); y++)
				{
					Flight flight = new Flight();
					String Sflight = insets.get(y);
					//System.out.println(Sflight);
					String depart = Sflight.split("#")[0];
					String arrive = Sflight.split("#")[1];
					
					String[] departArray = depart.split(" ");
					
					String Ddate = "-";
					Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
					flight.setDepartureDate(Ddate);
					String Dtime = "-";
					Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
					flight.setDepartureTime(Dtime);
					
					flight.setFlightNo(depart.split("[()]")[3]);
					flight.setDepartureLocationCode(depart.split("[()]")[1]);
					
					String[] arriveArray = arrive.split(" ");
					String Adate = "-";
					Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
					String Atime = "-";
					Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
					flight.setArrivalDate(Adate);
					flight.setArrivalTime(Atime);
					flight.setArrivalLocationCode(arrive.split("[()]")[1]);
					//System.out.println(flight.getArrivalLocationCode());
					
					inflightlist.add(flight);
				}
				
				inbound.setInBflightlist(inflightlist);
				uiconfirmationpage.setInbound(inbound);
			}
			
			WebElement ratetable = driver.findElement(By.id("booking_rate_details_table"));
			ArrayList<WebElement> rateTRtags = new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
			//System.out.println("No of tr tags : "+rateTRtags.size());
			
			int passengercount = rateTRtags.size() - 6;
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();;
			for(int h=2; h<(2+passengercount); h++)
			{
				
				//System.out.println(i+=1);
				RatesperPassenger rate = new RatesperPassenger();
				ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
				
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
				{
					rate.setPassengertype("ADT");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
				{
					rate.setPassengertype("CHD");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
				{
					rate.setPassengertype("INF");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				
				ratelist.add(rate);
			}
			uiconfirmationpage.setRatelist(ratelist);
			
			int forward = 2 + passengercount;
			
			//System.out.println(rateTRtags.get(forward).getText());
			UIPriceInfo priceinfo = new UIPriceInfo();
			
			priceinfo.setTotalbefore(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
			priceinfo.setTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
			priceinfo.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
			priceinfo.setTotalCost(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
			
			WebElement totfareTable = driver.findElement(By.className("transfer_info_table"));
			//System.out.println("------------=================-----------------");
			//System.out.println(totfareTable.getText());
			String[] totfareArray = totfareTable.getText().split("\\n");
			//System.out.println(totfareArray[0].split("[()]")[0]);
			//priceinfo.setCurrencycode(totfareArray[0].split("[()]")[1]);
			priceinfo.setSubtotal(totfareArray[0].split(":")[1].trim());
			priceinfo.setTaxplusother(totfareArray[1].split(":")[1].trim());
			priceinfo.setTotal(totfareArray[2].split(":")[1].trim());
			priceinfo.setAmountprocess(totfareArray[3].split(" ")[4].trim());
			//System.out.println(totfareArray[9].split(" ")[5]);
			priceinfo.setAmountprocessAirline(totfareArray[4].split(" ")[5]);
			
			uiconfirmationpage.setUisummarypay(priceinfo);
			try
			{
				UICreditCardPayInfo cardinfo = new UICreditCardPayInfo();
			
				cardinfo.setReferenceNo(driver.findElement(By.id("paymentrefno")).getText());
				try {
					cardinfo.setMerchantTrackID(driver.findElement(By.id("paymentid")).getText());
				} catch (Exception e) {
					System.out.println("payment");
				}
				
				cardinfo.setPaymentId(driver.findElement(By.id("paymenttrackid")).getText());
				cardinfo.setAmount(driver.findElement(By.id("onlinecharge")).getText());
				uiconfirmationpage.setCreditcardpay(cardinfo);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Credit Card Info"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				WebElement cancellation = driver.findElement(By.id("table_cancellation_policies"));
				
				ArrayList<WebElement> cancellationTRtags = new ArrayList<WebElement>(cancellation.findElements(By.tagName("tr")));
				
				String flightdeadline = "";
				flightdeadline = cancellationTRtags.get(1).getText().split(": ")[1].trim();

				uiconfirmationpage.setCancellationdate(flightdeadline);
				
				String packagecancel = "";
				packagecancel = cancellationTRtags.get(cancellationTRtags.size()-1).getText();
				packagecancel = packagecancel.split(":")[1].trim();
				uiconfirmationpage.setPackageDeadline(packagecancel);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String a = "";
				uiconfirmationpage.setCancellationdate(a);
				String b = "";
				uiconfirmationpage.setPackageDeadline(b);
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page CalcellationDate"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				//Main customer
				WebElement mainCus = driver.findElement(By.id("table_customer_details1"));
				ArrayList<WebElement> mainCusTRlist = new ArrayList<WebElement>(mainCus.findElements(By.tagName("tr")));
				
				String fname = mainCusTRlist.get(0).findElement(By.id("cusfirstname")).getText();
				String lname = mainCusTRlist.get(0).findElement(By.id("cuslastname")).getText();
				String phoneNo = mainCusTRlist.get(1).findElement(By.id("custelephone")).getText();
				String emergencyNo = mainCusTRlist.get(1).findElement(By.id("cusaltphone")).getText();
				String email1 = mainCusTRlist.get(2).findElement(By.id("cusemail1")).getText();
				String email2 = mainCusTRlist.get(2).findElement(By.id("cusemail2")).getText();
				String address1 = mainCusTRlist.get(3).findElement(By.id("cusaddress1")).getText();
				String Country = mainCusTRlist.get(4).findElement(By.id("cuscountry")).getText();
				String state = mainCusTRlist.get(4).findElement(By.id("cusstate")).getText();
				String city = mainCusTRlist.get(5).findElement(By.id("cuscity")).getText();
				String postalcode = "";
				try
				{
					ArrayList<WebElement> postcode = new ArrayList<WebElement>(mainCusTRlist.get(5).findElements(By.id("cuspostcode")));
					postalcode = postcode.get(1).getText().trim();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				Traveler maincustomer = new Traveler();
				Address address = new Address();
				maincustomer.setGivenName(fname);
				maincustomer.setSurname(lname);
				maincustomer.setPhoneNumber(phoneNo);
				maincustomer.setEmergencyNo(emergencyNo);
				maincustomer.setEmail(email1);
				maincustomer.setAltemail(email2);
				
				address.setAddressStreetNo(address1);
				address.setAddressCountry(Country);
				address.setStateProv(state);
				address.setAddressCity(city);
				address.setPostalCode(postalcode);
				
				maincustomer.setAddress(address);
				
				uiconfirmationpage.setMaincustomer(maincustomer);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation page Customer Dertails"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			//Passengers
			ArrayList<Traveler> adults = new ArrayList<Traveler>();
			ArrayList<Traveler> children = new ArrayList<Traveler>();
			ArrayList<Traveler> infants = new ArrayList<Traveler>();
			WebElement passengers = driver.findElement(By.className("table_flight_passenger_details"));
			ArrayList<WebElement> passengersTRlist = new ArrayList<WebElement>(passengers.findElements(By.tagName("tr")));
			
			for(int y=1; y<passengersTRlist.size(); y++)
			{
				//System.out.println(passengersTRlist.size());
				String type = passengersTRlist.get(y).findElement(By.className("Tlightblue")).getText();
				
				if(type.contains("Adult"))
				{
					Traveler adult = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					adult.setPassengertypeCode("ADT");
					adult.setNamePrefixTitle(title);
					adult.setGivenName(gname);
					adult.setSurname(sname);
					adult.setPhoneNumber(contact);
					adult.setFrequent_Flyer(frequentflyer);
					adult.setFrequent_Flyer_No(frequentflyerNo);
					adult.setETicket_No(eticketNo);
					
					adults.add(adult);
				}
				else if(type.contains("Child"))
				{
					Traveler child = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();
					
					child.setPassengertypeCode("CHD");
					child.setNamePrefixTitle(title);
					child.setGivenName(gname);
					child.setSurname(sname);
					child.setPhoneNumber(contact);
					child.setFrequent_Flyer(frequentflyer);
					child.setFrequent_Flyer_No(frequentflyerNo);
					child.setETicket_No(eticketNo);
					
					children.add(child);
				}
				else if(type.contains("Infant"))
				{
					Traveler infant = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					infant.setPassengertypeCode("INF");
					infant.setNamePrefixTitle(title);
					infant.setGivenName(gname);
					infant.setSurname(sname);
					infant.setPhoneNumber(contact);
					infant.setFrequent_Flyer(frequentflyer);
					infant.setFrequent_Flyer_No(frequentflyerNo);
					infant.setETicket_No(eticketNo);
					
					infants.add(infant);
				}
			}//end of for
			
			uiconfirmationpage.setAdult(adults);
			uiconfirmationpage.setChildren(children);
			uiconfirmationpage.setInfant(infants);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page error"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		return uiconfirmationpage;	
	}

	public WebDriver Fill_Reservation_Details(WebDriver driver, ReservationInfo fillObj, SearchObject sobj)
	{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		
		/*try {
			if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
			{
				driver.findElement(By.id("user_id")).sendKeys(Propertymap.get("portal.username"));
				driver.findElement(By.id("user_password")).sendKeys(Propertymap.get("portal.password"));
				driver.findElement(By.id("activate_mg")).click();
				Thread.sleep(4000);
			}
		} catch (Exception e) {
			System.out.println("Active manager override failed");
		}*/
		
		/*try {
			driver.findElement(By.id("cancellation")).click();
		} catch (Exception e) {
			System.out.println("didnt click the cancellation");
		}
		*/
		
		int adults		= fillObj.getAdult().size();
		int children	= fillObj.getChildren().size();
		int infants		= fillObj.getInfant().size();
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 10,5);
			Traveler main = fillObj.getMaincustomer();
		
			try {
				driver.findElement(By.id("cusFName")).clear();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusFName")));
				driver.findElement(By.id("cusFName")).sendKeys(main.getGivenName());
				driver.findElement(By.id("cusLName")).clear();
				driver.findElement(By.id("cusLName")).sendKeys(main.getSurname());
				driver.findElement(By.id("cusAdd1")).clear();
				driver.findElement(By.id("cusAdd1")).sendKeys(main.getAddress().getAddressStreetNo());
				driver.findElement(By.id("cusAdd2")).clear();
				driver.findElement(By.id("cusAdd2")).sendKeys(main.getAddress().getAddressStreetNo()+"123");
				driver.findElement(By.id("cusCity")).clear();
				driver.findElement(By.id("cusCity")).sendKeys(main.getAddress().getAddressCity());
				new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(main.getAddress().getAddressCountry());
				
				
				/*try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusState")));
					try {
						if(driver.findElement(By.id("cusState")).isDisplayed())
						{
							new Select(driver.findElement(By.id("cusState"))).selectByVisibleText(main.getAddress().getStateProv());
						}
						
					} catch (Exception e) {
						System.out.println("State is not entered");
					}
					
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusZip")));
					try {
						if(driver.findElement(By.id("cusZip")).isDisplayed())
						{
							driver.findElement(By.id("cusZip")).sendKeys(main.getAddress().getCountryZip());
						}
					} catch (Exception e) {
						System.out.println("Zip code is not entered");
					}
				} catch (Exception e) {
					System.out.println("State is not entered");
				}*/
				
				driver.findElement(By.id("cusareacodetext")).clear();
				driver.findElement(By.id("cusareacodetext")).sendKeys("11");
				driver.findElement(By.id("cusPhonetext")).clear();
				driver.findElement(By.id("cusPhonetext")).sendKeys(main.getPhoneNumber());
				driver.findElement(By.id("cusEmail")).clear();
				driver.findElement(By.id("cusEmail")).sendKeys(main.getEmail());
				driver.findElement(By.id("cusPhoneemgtext")).clear();
				driver.findElement(By.id("cusPhoneemgtext")).sendKeys(main.getPhoneNumber());
				
			} catch (Exception e) {
				System.out.println("Main customer information fill fail");
			}

			try {
				for(int a = 0; a<adults; a++)
				{
					try {
						new Select(driver.findElement(By.id("tfpi_adult_title_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
					} catch (Exception e) {
						try {
							new Select(driver.findElement(By.id("tfpi_adult_title_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
						} catch (Exception e2) {
							System.out.println("Adult title is not selected");	
						}
					}
						
					driver.findElement(By.id("tfpi_adult_first_name_"+a+"")).clear();
					driver.findElement(By.id("tfpi_adult_first_name_"+a+"")).sendKeys(fillObj.getAdult().get(a).getGivenName());
					driver.findElement(By.id("tfpi_adult_last_name_"+a+"")).clear();
					driver.findElement(By.id("tfpi_adult_last_name_"+a+"")).sendKeys(fillObj.getAdult().get(a).getSurname());
					driver.findElement(By.id("ftpi_adult_contact_information_"+a+"")).clear();
					driver.findElement(By.id("ftpi_adult_contact_information_"+a+"")).sendKeys(fillObj.getAdult().get(a).getPhoneNumber());
					driver.findElement(By.id("ftpi_adult_passport_number_"+a+"")).clear();
					driver.findElement(By.id("ftpi_adult_passport_number_"+a+"")).sendKeys(fillObj.getAdult().get(a).getPassportNo());
					
					((JavascriptExecutor)driver).executeScript("$('#adultPassportExpDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassportExpDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#adultPassportIssueDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassprtIssuDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#adultDateOfBirth_Air_"+a+"').val('"+fillObj.getAdult().get(a).getBirthDay().trim()+"');");
						
					new Select(driver.findElement(By.id("ftpi_adult_sex_"+a+""))).selectByValue(fillObj.getAdult().get(a).getGender());
					new Select(driver.findElement(By.id("ftpi_adult_nationality_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNationality());
					new Select(driver.findElement(By.id("ftpi_adult_pic_country_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getPassportIssuCountry());
				}
			} catch (Exception e) {
				System.out.println("Adult information fill fail");
			}
			
			try {
				for(int c=0; c<children; c++)
				{
					new Select(driver.findElement(By.id("tfpi_child_title_"+c+""))).selectByValue(fillObj.getChildren().get(c).getNamePrefixTitle());
					driver.findElement(By.id("tfpi_child_first_name_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_first_name_"+c+"")).sendKeys(fillObj.getChildren().get(c).getGivenName());
					driver.findElement(By.id("tfpi_child_last_name_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_last_name_"+c+"")).sendKeys(fillObj.getChildren().get(c).getSurname());
					driver.findElement(By.id("tfpi_child_contact_information_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_contact_information_"+c+"")).sendKeys(fillObj.getChildren().get(c).getPhoneNumber());
					driver.findElement(By.id("tfpi_child_passport_number_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_passport_number_"+c+"")).sendKeys(fillObj.getChildren().get(c).getPassportNo());
						
					((JavascriptExecutor)driver).executeScript("$('#childPassportExpDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassportExpDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#childPassportIssueDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassprtIssuDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#childDateOfBirth_Air_"+c+"').val('"+fillObj.getChildren().get(c).getBirthDay().trim()+"');");
						
					new Select(driver.findElement(By.id("tfpi_child_sex_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getGender());
					new Select(driver.findElement(By.id("tfpi_child_nationality_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getNationality());
					new Select(driver.findElement(By.id("tfpi_child_pic_country_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getPassportIssuCountry());
				}
			} catch (Exception e) {
				System.out.println("Children information fill fail");
			}	
			
			try {
				for(int i = 0; i<infants; i++)
				{
					new Select(driver.findElement(By.id("tfpi_infant_title_"+i+""))).selectByVisibleText(fillObj.getInfant().get(i).getNamePrefixTitle());
					driver.findElement(By.id("tfpi_infant_first_name_"+i+"")).clear();
					driver.findElement(By.id("tfpi_infant_first_name_"+i+"")).sendKeys(fillObj.getInfant().get(i).getGivenName());
					driver.findElement(By.id("tfpi_infant_last_name_"+i+"")).clear();
					driver.findElement(By.id("tfpi_infant_last_name_"+i+"")).sendKeys(fillObj.getInfant().get(i).getSurname());
					
					((JavascriptExecutor)driver).executeScript("$('#infantBirthDate_Air_"+i+"').val('"+fillObj.getInfant().get(i).getBirthDay().trim()+"');");
					new Select(driver.findElement(By.id("tfpi_infant_sex_"+i+""))).selectByVisibleText(fillObj.getInfant().get(i).getGender());
					driver.findElement(By.id("tfpi_infant_passport_number_"+i+"")).sendKeys(fillObj.getInfant().get(i).getPassportNo());
				}
			} catch (Exception e) {
				System.out.println("Infant informatin fill fail");
			}	
		}
		catch(Exception e)
		{
			System.out.println("Error in passanger details filling");
		}
		
		try {
			if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
			{
				driver.findElement(By.id("user_id")).sendKeys(Propertymap.get("portal.username"));
				driver.findElement(By.id("user_password")).sendKeys(Propertymap.get("portal.password"));
				driver.findElement(By.id("activate_mg")).click();
				Thread.sleep(4000);
			}
		} catch (Exception e) {
			System.out.println("Active manager override failed");
		}
		
		try {
			if( !sobj.isApplyDiscount() && !sobj.isQuotation() && sobj.isApplyDiscountAtPayPg() )
			{
				driver.findElement(By.id("discount_coupon_user_id")).sendKeys(Propertymap.get("DiscountCouponNo"));
				driver.findElement(By.id("search_btns")).click();
				
				if(driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
				{
					this.setDiscountText(driver.findElement(By.id("dialog-alert-message-WJ_22")).getText().trim());
					try {
						driver.findElement(By.className("ui-button-text")).click();
					} catch (Exception e) {
						System.out.println("Discount coupon message box didnt close");
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Discount application faild");
		}
		
		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 7000);
			if(sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			{
				try {
					if(!driver.findElement(By.id("pay_online")).isSelected())
					{
						driver.findElement(By.id("pay_online")).click();
					}
				} catch (Exception e) {
					System.out.println("Pay online is selected is clicked");
				}
			}
			else if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
			{
				driver.findElement(By.id("pay_offline")).click();
				
				Thread.sleep(5000);
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("paymentGuaranteed")));
				driver.findElement(By.id("paymentGuaranteed")).click();
				
				Thread.sleep(3000);
				new Select(driver.findElement(By.id("payment_method_1"))).selectByValue("CS");
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("payment_reference")));
				driver.findElement(By.id("payment_reference")).sendKeys("Cash1123");
			}
		} catch (Exception e) {
			System.out.println("Payment mode selection failed");
		}
		
		try {
			driver.findElement(By.id("customer_mail_send_yes")).click();
			driver.findElement(By.id("voucher_mail_send_yes")).click();
			driver.findElement(By.id("supplier_mail_send_yes")).click();
			driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div.wrapper div.content_body_wrapper.group div.content_full_col_wrapper div.full_column div.rounded_widget table#table_send_mail.table_send_mail tbody tr td.amo_width_6 input#supplier_mail_send_yes")).click();
		} catch (Exception e) {
			
		try {
			driver.findElement(By.id("cancellation")).click();
		} catch (Exception et) {
			System.out.println("didnt click the cancellation");
		}	
		
		}
		try {
			driver.findElement(By.id("continue_submit")).click();
		} catch (Exception e) {
			System.out.println("Continue button click failed");
		}
		return driver;
	}

	public WebConfirmationPage getConfirmationPageQ(WebDriver driver, XMLPriceItinerary XMLSelectedFlight, HashMap<String, String> PMap ) throws WebDriverException, IOException
	{
		WebConfirmationPage uiconfirmationpage = new WebConfirmationPage(PMap);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		
		boolean twoway = false;
		if(XMLSelectedFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
		}
		
		try
		{
			String ReservationNo = driver.findElement(By.id("p_flight_booking_reservation_no_1")).getText().split(":")[1];
			String suppConfirmationNo = driver.findElement(By.id("p_flight_booking_supplier_confirmation_no_1")).getText().split(":")[1];
			
			uiconfirmationpage.setReservationNo(ReservationNo.trim());
			uiconfirmationpage.setAvailable(true);
			uiconfirmationpage.setSupplierConfirmationNo(suppConfirmationNo.trim());
			
			WebElement alloutbound = driver.findElement(By.id("table_flight_outbound_info"));
			String outbounddetails = alloutbound.getText();
			//System.out.println(alloutbound.getText());
			
			String[] outboundArray = outbounddetails.split("\\n");
			ArrayList<String> sets = new ArrayList<String>();
			ArrayList<Flight> outflightlist = new ArrayList<Flight>(); 
			
			for(int y=0; y<outboundArray.length; y++)
			{
				if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
				{
					String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
					sets.add(h);
				}
			}
			
			Outbound outbound = new Outbound();
			for(int y=0; y<sets.size(); y++)
			{
				Flight flight = new Flight();
				String Sflight = sets.get(y);
				String depart = Sflight.split("#")[0];
				String arrive = Sflight.split("#")[1];
				
				String[] departArray = depart.split(" ");
				
				//System.out.println(departArray[1]);
				flight.setDepartureDate(departArray[1]);
				flight.setDepartureTime(departArray[2]);
				
				/*String[] g = depart.split("[()]");
				for (int u=0; u<g.length; u++) {
					System.out.println(g[u]);
				}*/
				//System.out.println(depart);
				//System.out.println(depart.split("[()]")[0]);
				//System.out.println(depart.split("[()]")[1]);
				//System.out.println(depart.split("[()]")[2]);
				flight.setFlightNo(depart.split("[()]")[3]);
				flight.setDepartureLocationCode(depart.split("[()]")[1]);
				
				String[] arriveArray = arrive.split(" ");
				//System.out.println(arriveArray[1]);
				flight.setArrivalDate(arriveArray[2]);
				flight.setArrivalTime(arriveArray[3]);
				
				/*String[] g = arrive.split("[()]");
				for (int u=0; u<g.length; u++) 
				{
					//System.out.println(g[u]);
				}*/
				flight.setArrivalLocationCode(arrive.split("[()]")[1]);
				//System.out.println(flight.getArrivalLocationCode());
				
				outflightlist.add(flight);
				
			}
			outbound.setOutBflightlist(outflightlist);
			uiconfirmationpage.setOutbound(outbound);
			
			//Inbound
			WebElement allinbound = null;
			
			if(twoway)
			{
				Inbound inbound = new Inbound();
				allinbound = driver.findElement(By.id("table_flight_inbound_info"));
				//System.out.println();
				String inbounddetails = allinbound.getText();
				//System.out.println(inbounddetails);
				
				String[] inboundArray = inbounddetails.split("\\n");
				ArrayList<String> insets = new ArrayList<String>();
				ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
				
				for(int y=0; y<inboundArray.length; y++)
				{
					if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
					{
						String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
						insets.add(h);
					}
				}
				
				for(int y=0; y<insets.size(); y++)
				{
					Flight flight = new Flight();
					String Sflight = insets.get(y);
					//System.out.println(Sflight);
					String depart = Sflight.split("#")[0];
					String arrive = Sflight.split("#")[1];
					
					String[] departArray = depart.split(" ");
					
					flight.setDepartureDate(departArray[1]);
					flight.setDepartureTime(departArray[2]);
					
					String[] g = depart.split("[()]");
					for (int u=0; u<g.length; u++) {
						System.out.println(g[u]);
					}
					//System.out.println("????????????????????????????????"+depart);
					flight.setFlightNo(depart.split("[()]")[3]);
					flight.setDepartureLocationCode(depart.split("[()]")[1]);
					
					String[] arriveArray = arrive.split(" ");
					flight.setArrivalDate(arriveArray[2]);
					flight.setArrivalTime(arriveArray[3]);
					
					/*String[] g = arrive.split("[()]");
					for (int u=0; u<g.length; u++) 
					{
						//System.out.println(g[u]);
					}*/
					flight.setArrivalLocationCode(arrive.split("[()]")[1]);
					//System.out.println(flight.getArrivalLocationCode());
					
					inflightlist.add(flight);
				}
				
				inbound.setInBflightlist(inflightlist);
				uiconfirmationpage.setInbound(inbound);
			}
			
			WebElement ratetable = driver.findElement(By.id("booking_rate_details_table"));
			ArrayList<WebElement> rateTRtags = new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
			//System.out.println("No of tr tags : "+rateTRtags.size());
			
			int passengercount = rateTRtags.size() - 6;
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();;
			for(int h=2; h<(2+passengercount); h++)
			{
				
				//System.out.println(i+=1);
				RatesperPassenger rate = new RatesperPassenger();
				ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
				
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
				{
					rate.setPassengertype("ADT");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
				{
					rate.setPassengertype("CHD");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
				{
					rate.setPassengertype("INF");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				
				ratelist.add(rate);
			}
			uiconfirmationpage.setRatelist(ratelist);
			
			int forward = 2 + passengercount;
			
			//System.out.println(rateTRtags.get(forward).getText());
			UIPriceInfo priceinfo = new UIPriceInfo();
			
			priceinfo.setTotalbefore(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
			priceinfo.setTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
			priceinfo.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
			priceinfo.setTotalCost(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
			
			WebElement totfareTable = driver.findElement(By.className("transfer_info_table"));
			//System.out.println("------------=================-----------------");
			//System.out.println(totfareTable.getText());
			String[] totfareArray = totfareTable.getText().split("\\n");
			//System.out.println(totfareArray[0].split("[()]")[0]);
			//priceinfo.setCurrencycode(totfareArray[0].split("[()]")[1]);
			priceinfo.setSubtotal(totfareArray[0].split(":")[1].trim());
			priceinfo.setTaxplusother(totfareArray[1].split(":")[1].trim());
			priceinfo.setTotal(totfareArray[2].split(":")[1].trim());
			priceinfo.setAmountprocess(totfareArray[3].split(" ")[4].trim());
			//System.out.println(totfareArray[9].split(" ")[5]);
			priceinfo.setAmountprocessAirline(totfareArray[4].split(" ")[5]);
			
			uiconfirmationpage.setUisummarypay(priceinfo);
			try
			{
				UICreditCardPayInfo cardinfo = new UICreditCardPayInfo();
			
				cardinfo.setReferenceNo(driver.findElement(By.id("paymentrefno")).getText());
				try {
					cardinfo.setMerchantTrackID(driver.findElement(By.id("paymentid")).getText());
				} catch (Exception e) {
					System.out.println("payment");
				}
				
				cardinfo.setPaymentId(driver.findElement(By.id("paymenttrackid")).getText());
				cardinfo.setAmount(driver.findElement(By.id("onlinecharge")).getText());
				uiconfirmationpage.setCreditcardpay(cardinfo);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Credit Card Info"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				WebElement cancellation = driver.findElement(By.id("table_cancellation_policies"));
				
				ArrayList<WebElement> cancellationTRtags = new ArrayList<WebElement>(cancellation.findElements(By.tagName("tr")));
				
				String flightdeadline = "";
				flightdeadline = cancellationTRtags.get(1).getText().split(": ")[1].trim();

				uiconfirmationpage.setCancellationdate(flightdeadline);
				
				String packagecancel = "";
				packagecancel = cancellationTRtags.get(cancellationTRtags.size()-1).getText();
				packagecancel = packagecancel.split(":")[1].trim();
				uiconfirmationpage.setPackageDeadline(packagecancel);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String a = "";
				uiconfirmationpage.setCancellationdate(a);
				String b = "";
				uiconfirmationpage.setPackageDeadline(b);
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page CalcellationDate"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				//Main customer
				WebElement mainCus = driver.findElement(By.id("table_customer_details1"));
				ArrayList<WebElement> mainCusTRlist = new ArrayList<WebElement>(mainCus.findElements(By.tagName("tr")));
				
				String fname = mainCusTRlist.get(0).findElement(By.id("cusfirstname")).getText();
				String lname = mainCusTRlist.get(0).findElement(By.id("cuslastname")).getText();
				String phoneNo = mainCusTRlist.get(1).findElement(By.id("custelephone")).getText();
				String emergencyNo = mainCusTRlist.get(1).findElement(By.id("cusaltphone")).getText();
				String email1 = mainCusTRlist.get(2).findElement(By.id("cusemail1")).getText();
				String email2 = mainCusTRlist.get(2).findElement(By.id("cusemail2")).getText();
				String address1 = mainCusTRlist.get(3).findElement(By.id("cusaddress1")).getText();
				String Country = mainCusTRlist.get(4).findElement(By.id("cuscountry")).getText();
				String state = mainCusTRlist.get(4).findElement(By.id("cusstate")).getText();
				String city = mainCusTRlist.get(5).findElement(By.id("cuscity")).getText();
				String postalcode = "";
				try
				{
					ArrayList<WebElement> postcode = new ArrayList<WebElement>(mainCusTRlist.get(5).findElements(By.id("cuspostcode")));
					postalcode = postcode.get(1).getText().trim();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				Traveler maincustomer = new Traveler();
				Address address = new Address();
				maincustomer.setGivenName(fname);
				maincustomer.setSurname(lname);
				maincustomer.setPhoneNumber(phoneNo);
				maincustomer.setEmergencyNo(emergencyNo);
				maincustomer.setEmail(email1);
				maincustomer.setAltemail(email2);
				
				address.setAddressStreetNo(address1);
				address.setAddressCountry(Country);
				address.setStateProv(state);
				address.setAddressCity(city);
				address.setPostalCode(postalcode);
				
				maincustomer.setAddress(address);
				
				uiconfirmationpage.setMaincustomer(maincustomer);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation page Customer Dertails"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			//Passengers
			ArrayList<Traveler> adults = new ArrayList<Traveler>();
			ArrayList<Traveler> children = new ArrayList<Traveler>();
			ArrayList<Traveler> infants = new ArrayList<Traveler>();
			WebElement passengers = driver.findElement(By.className("table_flight_passenger_details"));
			ArrayList<WebElement> passengersTRlist = new ArrayList<WebElement>(passengers.findElements(By.tagName("tr")));
			
			for(int y=1; y<passengersTRlist.size(); y++)
			{
				//System.out.println(passengersTRlist.size());
				String type = passengersTRlist.get(y).findElement(By.className("Tlightblue")).getText();
				
				if(type.contains("Adult"))
				{
					Traveler adult = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					adult.setPassengertypeCode("ADT");
					adult.setNamePrefixTitle(title);
					adult.setGivenName(gname);
					adult.setSurname(sname);
					adult.setPhoneNumber(contact);
					adult.setFrequent_Flyer(frequentflyer);
					adult.setFrequent_Flyer_No(frequentflyerNo);
					adult.setETicket_No(eticketNo);
					
					adults.add(adult);
				}
				else if(type.contains("Child"))
				{
					Traveler child = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();
					
					child.setPassengertypeCode("CHD");
					child.setNamePrefixTitle(title);
					child.setGivenName(gname);
					child.setSurname(sname);
					child.setPhoneNumber(contact);
					child.setFrequent_Flyer(frequentflyer);
					child.setFrequent_Flyer_No(frequentflyerNo);
					child.setETicket_No(eticketNo);
					
					children.add(child);
				}
				else if(type.contains("Infant"))
				{
					Traveler infant = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					infant.setPassengertypeCode("INF");
					infant.setNamePrefixTitle(title);
					infant.setGivenName(gname);
					infant.setSurname(sname);
					infant.setPhoneNumber(contact);
					infant.setFrequent_Flyer(frequentflyer);
					infant.setFrequent_Flyer_No(frequentflyerNo);
					infant.setETicket_No(eticketNo);
					
					infants.add(infant);
				}
			}//end of for
			
			uiconfirmationpage.setAdult(adults);
			uiconfirmationpage.setChildren(children);
			uiconfirmationpage.setInfant(infants);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page error"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		return uiconfirmationpage;	
	}

}
